use core::hash::Hash;
use std::{collections::HashMap, thread, time::Duration, env};

fn main() {
    let mut intensity = 10;
    let mut random = 7;
    let args: Vec<String> = env::args().collect();

    if args.len() == 3 {
        if let Ok(num) = args[1].parse() {
            intensity = num
        };

        if let Ok(num) = args[2].parse() {
            random = num
        };
    }
    
    generate_workout(intensity, random);
}

// A Hashmap stores cached return values for previous inputs to the closure.
struct Cacher<T, X, Y>
where
    T: Fn(X) -> Y,
    X: Eq + Copy + Hash,
{
    calculation: T,
    value: HashMap<X, Y>,
}

impl<T, X, Y> Cacher<T, X, Y>
where
    T: Fn(X) -> Y,
    X: Eq + Copy + Hash,
{
    // Create a new cacher for a given T(X) -> Y closure.
    fn new(calculation: T) -> Cacher<T, X, Y> {
        Cacher {
            calculation,
            value: HashMap::new(),
        }
    }

    // Take a reference to the argument.
    // This way it requires neither Copy nor Clone.
    // The result value is owned by the hashmap, return a reference to it.
    // This prevents accidental data corruption by the caller.
    fn value(&mut self, arg: X) -> &Y {
        let calculation = &self.calculation;
        self.value.entry(arg).or_insert_with(|| calculation(arg))
    }
}

// Example function from the Rust book to test the cacher.
fn generate_workout(intensity: u32, random_number: u32) {
    let mut expensive_result = Cacher::new(|num| {
        println!("calculating slowly...");
        thread::sleep(Duration::from_secs(2));
        num
    });

    if intensity < 25 {
        println!("Today, do {} pushups!", expensive_result.value(intensity));
        println!("Next, do {} situps!", expensive_result.value(intensity));
    } else if random_number == 3 {
        println!("Take a break today! Remember to stay hydrated!");
    } else {
        println!(
            "Today, run for {} minutes!",
            expensive_result.value(intensity)
        );
    }
}
